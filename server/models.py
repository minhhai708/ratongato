from django.db import models
from django.core.exceptions import ValidationError
class User(models.Model):
    userName = models.CharField(max_length=128, unique=True)
    pwd = models.CharField(max_length=128)
    def __unicode__(self):  #For Python 2, use __str__ on Python 3
        ret = "User: " + str(userName)
        return ret
class Game(models.Model):
    catUser = models.ForeignKey(User, related_name='game_catUsers')
    mouseUser = models.ForeignKey(User)
    cat1= models.IntegerField(default=57, null=False)
    cat2= models.IntegerField(default=59, null=False)
    cat3= models.IntegerField(default=61, null=False)
    cat4= models.IntegerField(default=63, null=False)
    mouse= models.IntegerField(default=3, null=False)
    catTurn = models.IntegerField(default=1, null=False)
def validateMove(value):
    if value > 63 or value < 0:
        raise ValidationError('Invalid move')
    if value%2 == 0:
        raise ValidationError('Invalid move')

class Move(models.Model):
    origin= models.IntegerField(default=3, null=False)
    target = models.IntegerField(default=1, null=False, validators=[validateMove])
    game = models.ForeignKey(Game)
    def __unicode__(self):  #For Python 2, use __str__ on Python 3
        return self
# Create your models here.
